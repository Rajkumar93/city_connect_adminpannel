import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  name: string;
  user: any;
  userpic: string;
  constructor() { }

  ngOnInit() {
    if (localStorage.getItem('user')) {
      this.user = JSON.parse(localStorage.getItem('user'));
      console.log(this.user);
      this.name = this.user.firstName + ' ' + this.user.lastName;
      this.userpic = this.user.photoURL;
    }
  }

}
