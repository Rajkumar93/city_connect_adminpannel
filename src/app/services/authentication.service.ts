import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable()
export class AuthenticationService {
  constructor(private http: HttpClient) { }

  login(loginInput) {
    return this.http.post<any>(`${environment.apiUrl}/api/login/`, loginInput)
      .pipe(map(user => {
        console.log(user.logindetails);
        // login successful if there's a jwt token in the response
        if (user && user.is_success == 1 && user.logindetails.sessionId) {/*&& user.token*/
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(user.logindetails.sessionId));
          localStorage.setItem('user', JSON.stringify(user.logindetails));
        }
        return user;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    localStorage.removeItem('user');
  }
}
