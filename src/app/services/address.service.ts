import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { environment } from "../../environments/environment";

@Injectable()
export class AddressService {
  constructor(private http: HttpClient) { }

  getCities() {
    console.log('config value');
    console.log(`${environment.apiUrl}`);
    return this.http.get<any>(`${environment.apiUrl}/api/cities`)
      .pipe(map(agents => {
        return agents;
      }));
  }

  getStates() {
    console.log('config value');
    console.log(`${environment.apiUrl}`);
    return this.http.get<any>(`${environment.apiUrl}/api/states`)
      .pipe(map(agents => {
        return agents;
      }));
  }
}
