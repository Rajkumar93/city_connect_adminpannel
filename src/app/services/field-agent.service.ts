import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map, retry } from 'rxjs/operators';
import { environment } from "../../environments/environment";

@Injectable()
export class FieldAgentService {
  constructor(private http: HttpClient) { }

  getAgents() {
    console.log('config value');
    console.log(`${environment.apiUrl}`);
    return this.http.get<any>(`${environment.apiUrl}/api/fieldAgentDetails`)
      .pipe(map(agents => {
        console.log(agents);
        return agents;
      }));
  }

  getAgentbyId(id) {
    console.log('config value');
    console.log(`${environment.apiUrl}`);
    return this.http.get<any>(`${environment.apiUrl}/api/fieldAgentDetails/` + id)
      .pipe(map(agents => {
        return agents;
      }));
  }

  registerAgent(agent: any) {
    console.log('Inside Register FieldAgent');
    console.log(agent);
    return this.http.post(`${environment.apiUrl}/api/fieldAgentDetails`, agent)
      .pipe(map(agent => {
        //return JSON.stringify(agent);
        console.log('agent');
        console.log(agent);
      }))
  }

  updateAgent(agent: any) {
    return this.http.put(`${environment.apiUrl}/api/fieldAgentDetails` + agent.teamId, agent)
      .pipe(map(agent => {
        //return JSON.stringify(agent);
        return agent;
      }))
  }

  deleteAgent(id: number) {
    return this.http.delete(`${environment.apiUrl}/api/fieldAgentDetails` + id);
    //.pipe(map())
  }

}
