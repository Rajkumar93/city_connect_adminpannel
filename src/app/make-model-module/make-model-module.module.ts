import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MakeModelRoutingModule } from './make-model.routing.module'

import { MakersListComponent } from './makers-list/makers-list.component';
import { AddNewMakerComponent } from './add-new-maker/add-new-maker.component';
import { ModelListComponent } from './model-list/model-list.component';
import { AddNewModelComponent } from './add-new-model/add-new-model.component';

@NgModule({
  imports: [
    CommonModule,
    MakeModelRoutingModule
  ],
  declarations: [MakersListComponent, AddNewMakerComponent, ModelListComponent, AddNewModelComponent]
})
export class MakeModelModuleModule { }
