import { Component, OnInit } from '@angular/core';

declare const $;
@Component({
  selector: 'app-model-list',
  templateUrl: './model-list.component.html',
  styleUrls: ['./model-list.component.css']
})
export class ModelListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#example2').DataTable();
  }

}
