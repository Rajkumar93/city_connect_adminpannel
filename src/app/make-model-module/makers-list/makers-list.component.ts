import { Component, OnInit } from '@angular/core';

declare const $;
@Component({
  selector: 'app-makers-list',
  templateUrl: './makers-list.component.html',
  styleUrls: ['./makers-list.component.css']
})
export class MakersListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#example2').DataTable();
  }

}
