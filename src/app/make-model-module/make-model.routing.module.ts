import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MakersListComponent } from './makers-list/makers-list.component';
import { AddNewMakerComponent } from './add-new-maker/add-new-maker.component';
import { ModelListComponent } from './model-list/model-list.component';
import { AddNewModelComponent } from './add-new-model/add-new-model.component';




const appRoutes: Routes = [
 { path: 'maker_list', component: MakersListComponent },
 { path: 'add_new_maker', component: AddNewMakerComponent },
 { path: 'model_list', component: ModelListComponent },
 { path: 'add_new_model', component: AddNewModelComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})

export class MakeModelRoutingModule { }
