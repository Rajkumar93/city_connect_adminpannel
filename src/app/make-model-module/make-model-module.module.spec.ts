import { MakeModelModuleModule } from './make-model-module.module';

describe('MakeModelModuleModule', () => {
  let makeModelModuleModule: MakeModelModuleModule;

  beforeEach(() => {
    makeModelModuleModule = new MakeModelModuleModule();
  });

  it('should create an instance', () => {
    expect(makeModelModuleModule).toBeTruthy();
  });
});
