import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewMakerComponent } from './add-new-maker.component';

describe('AddNewMakerComponent', () => {
  let component: AddNewMakerComponent;
  let fixture: ComponentFixture<AddNewMakerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewMakerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewMakerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
