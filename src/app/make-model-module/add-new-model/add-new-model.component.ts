import { Component, OnInit } from '@angular/core';

declare const $;
@Component({
  selector: 'app-add-new-model',
  templateUrl: './add-new-model.component.html',
  styleUrls: ['./add-new-model.component.css']
})
export class AddNewModelComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.js-example-basic-single').select2();
  }

}
