import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { FieldAgentRoutingModule } from './field-agent.routing.module';

import { FieldAgentComponent } from './field-agent/field-agent.component';
import { AddFieldAgentComponent } from './add-field-agent/add-field-agent.component';
//import { FieldAgent, AddressDetails } from '../models/FieldAgent';



@NgModule({
  declarations: [
    FieldAgentComponent,
    AddFieldAgentComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FieldAgentRoutingModule
  ],
  providers: [
    //FieldAgent,
    //AddressDetails,
  ],
})
export class FieldAgentModule { }
