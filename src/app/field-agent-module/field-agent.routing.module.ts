import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FieldAgentComponent } from './field-agent/field-agent.component';

import { AddFieldAgentComponent } from './add-field-agent/add-field-agent.component';



const appRoutes: Routes = [
  { path: 'fieldAgentList', component: FieldAgentComponent },
  { path: 'addFieldAgent', component: AddFieldAgentComponent },
  { path: 'addFieldAgent/:id', component: AddFieldAgentComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})

export class FieldAgentRoutingModule { }
