import { Component, OnInit } from '@angular/core';
import { FieldAgentService } from '../../services/field-agent.service';

declare let $;
@Component({
  selector: 'app-field-agent',
  templateUrl: './field-agent.component.html',
  styleUrls: ['./field-agent.component.css']
})
export class FieldAgentComponent implements OnInit {

  agents: any = [];

  constructor(private fieldAgentService: FieldAgentService) { }

  ngOnInit() {
    console.log('Inside Field Agent');
    this.fieldAgentService.getAgents()
      .subscribe(data => {
        console.log('FieldAgent Data');
        console.log(data);
        this.agents = data;
      });

    $('#example1').DataTable();


  }

  editAgent(id) {
    console.log(id);

  }


}
