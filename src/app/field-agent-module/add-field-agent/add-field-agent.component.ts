import { Component, OnInit } from '@angular/core';
import { AddressService } from '../../services/address.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

import { FieldAgent } from '../../models/FieldAgent';
import { FieldAgentService } from '../../services/field-agent.service';
import { ActivatedRoute } from '@angular/router';


declare let $;
@Component({
  selector: 'app-add-field-agent',
  templateUrl: './add-field-agent.component.html',
  styleUrls: ['./add-field-agent.component.css']
})
export class AddFieldAgentComponent implements OnInit {

  cities: any = [];
  states: any = [];
  submitted = false;
  country: string = '';
  agent = {
    firstName: '',
    lastName: '',
    phoneNo: '',
    isActive: false,
    email: '',
    addressDetails: {
      city: '',
      state: '',
      country: '',
      pinCode: '',
      adrLine1: ''

    }
  };
  agentId: number;
  eAgent: any = {};
  exampleForm: FormGroup;

  emailPattern: string = "[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$";

  constructor(private addressService: AddressService,
    private fieldAgentService: FieldAgentService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute) {
    
  }

  ngOnInit() {
    //iCheck for checkbox and radio inputs
    //$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    //  checkboxClass: 'icheckbox_minimal-blue',
    //  radioClass   : 'iradio_minimal-blue'
    //})
    ////Red color scheme for iCheck
    //$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
    //  checkboxClass: 'icheckbox_minimal-red',
    //  radioClass   : 'iradio_minimal-red'
    //})
    ////Flat red color scheme for iCheck
    //$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    //  checkboxClass: 'icheckbox_flat-orange',
    //  radioClass   : 'iradio_flat-orange'
    //})


    // load Cities
    //this.addressService.getCities()
    //  .subscribe(data => {
    //    this.cities = data;
    //  });

    //// load States
    //this.addressService.getStates()
    //  .subscribe(data => {
    //    this.states = data;
    //  });


    //--- Initiate Form ----
    this.exampleForm = new FormGroup({
      firstName: new FormControl(),
      lastName: new FormControl(),
      state: new FormControl(),
      city: new FormControl(),
      pinCode: new FormControl(),
      adrLine1: new FormControl(),
      phoneNo: new FormControl(),
      email: new FormControl(),
      isActive: new FormControl(),
    });

    this.createForm();
    
    this.agentId = +this.route.snapshot.paramMap.get('id');
    if (this.agentId > 0) {
      return this.fieldAgentService.getAgentbyId(this.agentId)
        .subscribe(data => {
          this.eAgent = data;
          console.log('this.eAgent');
          console.log(this.eAgent);
          //this.exampleForm.value.firstName = this.eAgent.firstName + ' ' + this.eAgent.lastName;
          console.log(this.exampleForm.value.firstName);
          this.bindFormValues(this.eAgent);
        });
    }

  }

  bindFormValues(eAgent) {
    this.exampleForm = this.formBuilder.group({
      firstName: eAgent.firstName,
      lastName: eAgent.lastName,
      state: eAgent.state,
      city: eAgent.city,
      pinCode: eAgent.pinCode,
      adrLine1: eAgent.adrLine1,
      phoneNo: eAgent.phoneNo,
      email: eAgent.email,
      isActive: eAgent.isActive,
    });
  }

  createForm() {
    this.exampleForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      state: ['', Validators.required],
      city: ['', Validators.required],
      pinCode: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.pattern("[0-9]{0,6}")])],
      adrLine1: ['', Validators.required],
      phoneNo: ['', Validators.compose([ Validators.required, Validators.minLength(10), Validators.pattern("[0-9]{0,10}")])],
      email: ['', Validators.compose([Validators.required, Validators.pattern(this.emailPattern)])],
      isActive: ['', Validators.required],
      //roleid: ['', Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.exampleForm.controls; }

  onSubmit() {
    this.submitted = true;
    console.log(this.exampleForm.get('isActive').value);
    // stop here if form is invalid
    //if (this.exampleForm.invalid) {
    //  return;
    //}
    this.agent.firstName = this.exampleForm.value.firstName;
    this.agent.lastName = this.exampleForm.value.lastName;
    this.agent.email = this.exampleForm.value.email;
    this.agent.addressDetails.adrLine1 = this.exampleForm.get('adrLine1').value;
    this.agent.addressDetails.city = this.exampleForm.get('city').value;
    this.agent.addressDetails.state = this.exampleForm.get('state').value;
    this.agent.addressDetails.pinCode = this.exampleForm.get('pinCode').value;
    this.agent.addressDetails.country = "India";
    this.agent.isActive = this.exampleForm.value.isActive == "1" ? true : false;
    console.log(this.agent.isActive);
    this.fieldAgentService.registerAgent(this.agent)
      .subscribe(data => {
        console.log(data);
      });
    alert('SUCCESS!! :-)');
  }

}
