import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFieldAgentComponent } from './add-field-agent.component';

describe('AddFieldAgentComponent', () => {
  let component: AddFieldAgentComponent;
  let fixture: ComponentFixture<AddFieldAgentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFieldAgentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFieldAgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
