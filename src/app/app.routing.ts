import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
//import { FieldAgentComponent } from './field-agent/field-agent.component';
//import { AddFieldAgentComponent } from './add-field-agent/add-field-agent.component';
// import { VehiclesComponent } from './vehicles/vehicles.component';
// import { AddVehicleTypeComponent } from './add-vehicle-type/add-vehicle-type.component';
import { LoginComponent } from './login/login.component';
// import { ReferralHistoryComponent } from './referral-history/referral-history.component';
import { AuthGuard } from './guards/auth.guard';

const appRoutes: Routes = [
  { path: '', redirectTo: 'dashboard', canActivate: [AuthGuard], pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  // { path: 'referral_history', component: ReferralHistoryComponent },

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
