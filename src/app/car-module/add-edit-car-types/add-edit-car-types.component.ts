import { Component, OnInit } from '@angular/core';

declare const $;
@Component({
  selector: 'app-add-edit-car-types',
  templateUrl: './add-edit-car-types.component.html',
  styleUrls: ['./add-edit-car-types.component.css']
})
export class AddEditCarTypesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.js-example-basic-multiple').select2();
  }

}
