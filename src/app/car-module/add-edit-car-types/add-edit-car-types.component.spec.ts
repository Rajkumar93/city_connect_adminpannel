import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditCarTypesComponent } from './add-edit-car-types.component';

describe('AddEditCarTypesComponent', () => {
  let component: AddEditCarTypesComponent;
  let fixture: ComponentFixture<AddEditCarTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddEditCarTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditCarTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
