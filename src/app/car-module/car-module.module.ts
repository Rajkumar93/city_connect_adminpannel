import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CarTypeComponent } from './car-type/car-type.component';
import { CarRoutingModule } from './car.routing.module';
import { AddEditCarTypesComponent } from './add-edit-car-types/add-edit-car-types.component';

@NgModule({
  imports: [
    CommonModule,
    CarRoutingModule
  ],
  declarations: [CarTypeComponent, AddEditCarTypesComponent]
})
export class CarModuleModule { }
