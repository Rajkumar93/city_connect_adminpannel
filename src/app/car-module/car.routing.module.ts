import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CarTypeComponent } from './car-type/car-type.component';
import { AddEditCarTypesComponent } from './add-edit-car-types/add-edit-car-types.component';




const appRoutes: Routes = [
 { path: 'car_type', component: CarTypeComponent },
 { path: 'add_edit_car_type', component: AddEditCarTypesComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})

export class CarRoutingModule { }
