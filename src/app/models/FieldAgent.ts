
export class AddressDetails {
  city: string;
  state: string;
  country: string;
  pinCode: string;
  adrLine1: string

}

export class FieldAgent {
  public faId: number;
  public firstName: string;
  public lastName: string;
  public phoneNo: string;
  public email: string;
  public isActive: boolean;
  public roleid: number;
  public address: AddressDetails;
}
