import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';

import { routing } from './app.routing';
import { FieldAgentModule } from './field-agent-module/field-agent.module';
import { VehiclesModule } from './vehicles-module/vehicles.module';
import { CarModuleModule } from './car-module/car-module.module';
import { ReferralModule } from './referral-module/referral.module';
import { MakeModelModuleModule } from './make-model-module/make-model-module.module';
import { CityFairsModuleModule } from './city-fairs-module/city-fairs-module.module'

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { AddressService } from './services/address.service';
import { FieldAgentService } from './services/field-agent.service';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './services/authentication.service';
import { AlertService } from './services/alert.service';
import { AuthGuard } from './guards/auth.guard';
import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';
// import { ReferralHistoryComponent } from './referral-history/referral-history.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    LoginComponent,
    // ReferralHistoryComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    FieldAgentModule,
    VehiclesModule,
    CarModuleModule,
    ReferralModule,
    MakeModelModuleModule,
    CityFairsModuleModule

  ],
  providers: [
    AuthGuard,
    AddressService,
    FieldAgentService,
    AuthenticationService,
    AlertService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule { }
