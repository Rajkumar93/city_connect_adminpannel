import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReferralHistoryComponent } from './referral-history/referral-history.component';
import { ViewReferralComponent } from './view-referral/view-referral.component';

// import {  } from '';



const appRoutes: Routes = [
  { path: 'referral_history', component: ReferralHistoryComponent },
  { path: 'view_referral', component: ViewReferralComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})

export class ReferralRoutingModule { }
