import { Component, OnInit } from '@angular/core';

declare const $;
@Component({
  selector: 'app-referral-history',
  templateUrl: './referral-history.component.html',
  styleUrls: ['./referral-history.component.css']
})
export class ReferralHistoryComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#example2').DataTable();
  }

}
