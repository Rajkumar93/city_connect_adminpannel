import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { ReferralRoutingModule } from './referral.routing.module';

import { ReferralHistoryComponent } from './referral-history/referral-history.component';
import { ViewReferralComponent } from './view-referral/view-referral.component';

@NgModule({
  declarations: [
    ReferralHistoryComponent,
    ViewReferralComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ReferralRoutingModule
  ],
})
export class ReferralModule { }
