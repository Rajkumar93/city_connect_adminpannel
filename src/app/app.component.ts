import { Component } from '@angular/core';
import { Router, NavigationEnd,NavigationStart, Event } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'city-connect';
  currentUrl="login";
  private sub:any;

  constructor(private router:Router){
    this.sub = router.events.subscribe((event : Event) => {
      // console.log(event);
      if (event instanceof NavigationStart ) {
      this.currentUrl = event.url;
      // console.log("current URl",this.currentUrl);
      }
      });
      
  }
}
