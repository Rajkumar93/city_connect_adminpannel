import { Component, OnInit } from '@angular/core';

declare let $;
@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#example2').DataTable();
  }

}
