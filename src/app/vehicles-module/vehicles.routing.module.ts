import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { VehiclesComponent } from './vehicles/vehicles.component';
import { AddVehicleTypeComponent } from './add-vehicle-type/add-vehicle-type.component';



const appRoutes: Routes = [
 { path: 'vehicles_type', component: VehiclesComponent },
 { path: 'add_vehicles_type', component: AddVehicleTypeComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})

export class VehiclesRoutingModule { }
