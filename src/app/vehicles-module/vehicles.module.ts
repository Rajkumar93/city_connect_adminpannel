import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { VehiclesRoutingModule } from './vehicles.routing.module';

import { VehiclesComponent } from './vehicles/vehicles.component';
import { AddVehicleTypeComponent } from './add-vehicle-type/add-vehicle-type.component';







@NgModule({
  declarations: [
    VehiclesComponent,
    AddVehicleTypeComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    VehiclesRoutingModule
  ],
  providers: [],
})
export class VehiclesModule { }
