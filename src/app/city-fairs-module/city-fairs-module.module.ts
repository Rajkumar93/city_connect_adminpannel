import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CityFairsRoutingModule } from './city-fairs.routing.module' 
import { CityListComponent } from './city-list/city-list.component';
import { AddCityComponent } from './add-city/add-city.component';
import { CityFairDetailsComponent } from './city-fair-details/city-fair-details.component';
import { from } from 'rxjs';

@NgModule({
  imports: [
    CommonModule,
    CityFairsRoutingModule
  ],
  declarations: [
    CityListComponent, 
    AddCityComponent, 
    CityFairDetailsComponent
  ]
})
export class CityFairsModuleModule { }
