import { Component, OnInit } from '@angular/core';

declare const $;
@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.css']
})
export class CityListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('#example2').DataTable();
  }

}
