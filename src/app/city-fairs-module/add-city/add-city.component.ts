import { Component, OnInit } from '@angular/core';

declare const $;
@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.css']
})
export class AddCityComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $('.js-example-basic-single').select2();
    $('.js-example-basic-multiple').select2();
  }

}
