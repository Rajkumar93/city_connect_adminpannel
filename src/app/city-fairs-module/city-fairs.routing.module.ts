import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CityListComponent } from './city-list/city-list.component';
import { AddCityComponent } from './add-city/add-city.component';
import { CityFairDetailsComponent } from './city-fair-details/city-fair-details.component';



const appRoutes: Routes = [
  { path: 'city_list', component: CityListComponent },
  { path: 'add_city', component: AddCityComponent },
  { path: 'city_fare_details', component: CityFairDetailsComponent },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(
      appRoutes,
      //{ enableTracing: true } // <-- debugging purposes only
    )
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})

export class CityFairsRoutingModule { }
