import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CityFairDetailsComponent } from './city-fair-details.component';

describe('CityFairDetailsComponent', () => {
  let component: CityFairDetailsComponent;
  let fixture: ComponentFixture<CityFairDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CityFairDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CityFairDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
